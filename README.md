Engine.ts
=========

A simple 2D game engine using TypeScript


How to Compile
--------------

Make sure you have TypeScript installed (http://typescriptlang.org) in order
to compile the engine.


* Compiling the engine (stand-alone)

        tsc engine/engine.ts -out engine.js


* Compile the sample

        tsc samples/tests/main.ts -out samples/tests/main.js